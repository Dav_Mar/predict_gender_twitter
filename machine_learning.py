#04/08/2018

from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score, confusion_matrix,classification_report
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction.text import HashingVectorizer
from sklearn.svm import LinearSVC
from sklearn_pandas import DataFrameMapper, CategoricalImputer
from sklearn.naive_bayes import MultinomialNB
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier



def RandomForest(X_train, y_train, X_test, y_test, n_estimators=50,criterion='gini',min_samples_split=27,min_samples_leaf=17,max_depth=10,n_jobs=-1,max_leaf_nodes=None,warm_start=False,oob_score = True,bootstrap=True):
          
    classifier=RandomForestClassifier         (n_estimators=n_estimators,criterion=criterion,min_samples_split=min_samples_split,min_samples_leaf=min_samples_leaf,max_depth=max_depth,n_jobs=n_jobs,max_leaf_nodes=max_leaf_nodes,warm_start=warm_start,oob_score = oob_score,bootstrap=bootstrap)
    classifier.fit(X_train, y_train) 
    y_pred = classifier.predict(X_test)  
    acc=accuracy_score(y_test, y_pred, normalize=True, sample_weight=None)
    print(confusion_matrix(y_test, y_pred))  
    print(classification_report(y_test, y_pred))
    return y_pred,acc



def TFID(text_train,max_df=0.99, max_features=8000,min_df=0.000075,use_idf=False, ngram_range=(1,2),stop_words = 'english'):

          tfidf_vectorizer= TfidfVectorizer(max_df=max_df, max_features=max_features,
                                              min_df=min_df,use_idf=use_idf, ngram_range=ngram_range,stop_words = stop_words)
          hash_vectorizer = HashingVectorizer()

          tfidf_matrix = tfidf_vectorizer.fit_transform(text_train)
          hash_matrix = hash_vectorizer.fit_transform(text_train)
          feature_names = tfidf_vectorizer.get_feature_names()
          return tfidf_matrix
        
        



def SVML(X_train, X_test, y_train, y_test, multi_class='crammer_singer'):
    clf_svm = LinearSVC(multi_class = multi_class)
    clf_svm.fit(X_train,y_train)
    pred_svm = clf_svm.predict(X_test)
    print(confusion_matrix(y_test, pred_svm ))  
    print(classification_report(y_test, pred_svm ))
    acc=accuracy_score(y_test, pred_svm, normalize=True, sample_weight=None)

    return pred_svm,acc


def NaiveBayes_MultiNom(X_train, X_test, y_train, y_test, alpha=.01):
    clf = MultinomialNB(alpha=alpha)
    clf.fit(X_train,y_train) # train the classifier
    pred = clf.predict(X_test) # test the classifier
    print(confusion_matrix(y_test, pred ))  
    print(classification_report(y_test, pred ))
    acc=accuracy_score(y_test, pred, normalize=True, sample_weight=None)

    return pred,acc


def DecisionTree(X_train, X_test, y_train, y_test,criterion = "gini", random_state = 100, max_depth=3, min_samples_leaf=5):
    clf_gini = DecisionTreeClassifier(criterion = criterion, random_state = random_state,max_depth=max_depth, min_samples_leaf=min_samples_leaf)
    clf_gini.fit(X_train, y_train)
    pred = clf_gini.predict(X_test) # test the classifier
    print(confusion_matrix(y_test, pred ))  
    print(classification_report(y_test, pred ))
    acc=accuracy_score(y_test, pred, normalize=True, sample_weight=None)

    return pred,acc




def textdata(f,out):
    mapper = DataFrameMapper([
         ('text_desc', TfidfVectorizer()),
         ('favourites_count', None),
         ('followers_count', None),
         ('friends_count', None),
      ('listed_count', None),
      ('statuses_count', None),
      ('age13_18', None),
      ('age19_22', None),
        ('age23_29', None),
        ('age30_65', None)
     ])


    features = mapper.fit_transform(f)
    categories = f[out]
    return features, categories
        
     